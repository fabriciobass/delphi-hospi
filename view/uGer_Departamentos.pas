unit uGer_Departamentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.ComCtrls, frxClass, frxDBSet,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TOperation  = (opNew, opEdit, opNavigate);
  
  TfrmDepartamentos = class(TForm)
    dsDepartamento: TDataSource;
    pgDepartamento: TPageControl;
    tabLista: TTabSheet;
    tabCadastro: TTabSheet;
    DBGrid1: TDBGrid;
    edtLocal: TEdit;
    edtNome: TEdit;
    edtID: TEdit;
    Panel1: TPanel;
    edtPesquisar: TEdit;
    btnPesquisar: TButton;
    Panel2: TPanel;
    btnEditar: TButton;
    btnExcluir: TButton;
    Panel3: TPanel;
    btnCancelar: TButton;
    btnGravar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnNovo: TButton;
    frxReport: TfrxReport;
    DBEmpregado: TfrxDBDataset;
    qryRelDepartamento: TFDQuery;
    qryRelDepartamentonm_empregado: TWideStringField;
    qryRelDepartamentoid_empregado: TIntegerField;
    qryRelDepartamentoid_departamento: TIntegerField;
    qryRelDepartamentonm_departamento: TWideStringField;
    qryRelDepartamentolocal: TWideStringField;
    btnImprimir: TButton;
    qryRelDepartamentosalario: TFMTBCDField;
    qryRelDepartamentodata_admissao: TDateField;
    qryRelDepartamentocomissao: TFMTBCDField;
    qryRelDepartamentonm_funcao: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnImprimirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FOperation  : TOperation;
  public
    { Public declarations }
    procedure searchDepartamento;
    procedure loadDepartamento;
    procedure Edit;
    procedure Delete;
    procedure Insert;
    procedure save;
    procedure Control(aOperation: TOperation);
    procedure limpaCampos;
  end;

var
  frmDepartamentos: TfrmDepartamentos;

implementation

{$R *.dfm}

uses uConexao, uDepartamentoController, uDepartamento;

procedure TfrmDepartamentos.btnGravarClick(Sender: TObject);
begin
  if edtNome.Text = '' then
  begin
    ShowMessage('Por favor, digite um Nome');
    Exit;
    edtNome.SetFocus;
  end;
  if edtLocal.Text = '' then
  begin
    ShowMessage('Por favor, digite um Local');
    Exit;
    edtLocal.SetFocus;
  end;
  save;
  Control(opNavigate);
  pgDepartamento.ActivePage := tabLista;
  limpaCampos;
end;

procedure TfrmDepartamentos.btnImprimirClick(Sender: TObject);
begin
  with qryRelDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('select e.nm_funcao, e.salario, e.comissao, e.data_admissao, e.nm_empregado, e.id_empregado, d.* from departamentos d ');
    SQL.Add('left join empregados e on e.cod_departamento = d.id_departamento ');
    SQL.Add(' where d.nm_departamento like :nm_departamento' );
    SQL.Add(' order by d.id_departamento');
    Params[0].AsString  := '%' + edtPesquisar.Text + '%';
    Open;

    if RecordCount > 0 then
    begin
      frxReport.Clear;
      frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
        '\relDepartamentos.fr3');
      frxReport.ShowReport;
    end else
      raise Exception.Create('N�o existem registros para impress�o!');
  end;
end;

procedure TfrmDepartamentos.Button2Click(Sender: TObject);
begin
  Edit;
end;

procedure TfrmDepartamentos.btnEditarClick(Sender: TObject);
begin
  FOperation  := opEdit;
  loadDepartamento;
  pgDepartamento.ActivePage := tabCadastro;
  Control(opEdit);  
end;

procedure TfrmDepartamentos.btnExcluirClick(Sender: TObject);
begin
  Delete;
end;

procedure TfrmDepartamentos.btnPesquisarClick(Sender: TObject);
begin
  searchDepartamento;
end;

procedure TfrmDepartamentos.btnCancelarClick(Sender: TObject);
begin
  searchDepartamento;
  pgDepartamento.ActivePage := tabLista;
  Control(opNavigate);
end;

procedure TfrmDepartamentos.btnNovoClick(Sender: TObject);
begin
  
  FOperation  := opNew;
  Control(opNew);
  limpaCampos;  
  pgDepartamento.ActivePage := tabCadastro;
  edtID.Text  := IntToStr(dmConexao.getNextID('departamentos'));
  edtNome.SetFocus;
  
end;

procedure TfrmDepartamentos.Control(aOperation: TOperation);
begin
  case aOperation of
    opNew, opEdit:
    begin
      edtNome.Enabled     := True;
      edtLocal.Enabled    := True;
      btnCancelar.Enabled := True;
      btnGravar.Enabled   := True;
      btnNovo.Enabled     := False;
      btnEditar.Enabled   := False;
      btnExcluir.Enabled  := False;
    end;
    opNavigate:
    begin
      edtNome.Enabled     := False;
      edtLocal.Enabled    := False;
      btnCancelar.Enabled := False;
      btnGravar.Enabled   := False;
      btnNovo.Enabled     := True;
      btnEditar.Enabled   := True;
      btnExcluir.Enabled  := True;
    end;
  end;  
end;

procedure TfrmDepartamentos.DBGrid1DblClick(Sender: TObject);
begin
  btnEditarClick(Self);
end;

procedure TfrmDepartamentos.Delete;
var
  oDepartamentoControl : TDepartamentoController;
  sErro: String;
begin
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    if (dmConexao.qryDepartamento.Active) and (dmConexao.qryDepartamento.RecordCount > 0) then
    begin
      if MessageDlg('Deseja exlcuir o registro?', mtConfirmation, [mbYes, mbNo], 0) = IDYES then
      begin
        if oDepartamentoControl.delete(dmConexao.qryDepartamento.FieldByName('id_departamento').AsInteger, sErro) = False then
          raise Exception.Create(sErro);
        oDepartamentoControl.Search(edtPesquisar.Text);
      end;
    end
    else
      raise Exception.Create('N�o existem registros a serem exclu�dos!');
        
  finally
    FreeAndNil(oDepartamentoControl);
  end;
end;

procedure TfrmDepartamentos.Edit;
var
  ODepartamento : TDepartamento;
  ODepartamentoControl : TDepartamentoController;
  sErro: string;
begin
  ODepartamento := TDepartamento.Create;
  ODepartamentoControl  := TDepartamentoController.Create;
  try
    with ODepartamento do
    begin
      Id_departamento := dmConexao.qryDepartamento.FieldByName('id_departamento').AsInteger;
      Nm_departamento := edtNome.Text;
      Local := edtLocal.Text;
    end;
    if ODepartamentoControl.Edit(ODepartamento, sErro) = False then
      raise Exception.Create(sErro);

  finally
    FreeAndNil(ODepartamento);
    FreeAndNil(ODepartamentoControl);
  end;
end;

procedure TfrmDepartamentos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_F2: btnNovoClick(Self);
    VK_F3: btnEditarClick(Self);
    VK_F4: btnExcluirClick(Self);
    VK_F5: btnGravarClick(Self);
    VK_F6: btnCancelarClick(Self);
    VK_ESCAPE: Close;
  end;
end;

procedure TfrmDepartamentos.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;

end;

procedure TfrmDepartamentos.FormShow(Sender: TObject);
begin
  pgDepartamento.ActivePage := tabLista;
  Control(opNavigate);
end;

procedure TfrmDepartamentos.Insert;
var
  oDepartamento : TDepartamento;
  oDepartamentoControl : TDepartamentoController;
  sErro: string;
begin
  oDepartamento := TDepartamento.Create;
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    with oDepartamento do
    begin  
      Id_departamento := 0;
      Nm_departamento := edtNome.Text;
      Local := edtLocal.Text;
    end;
    if oDepartamentoControl.Insert(oDepartamento, sErro) = False then
      raise Exception.Create(sErro);
    
  finally
    FreeAndNil(oDepartamento);
    FreeAndNil(oDepartamentoControl);    
  end;
end;

procedure TfrmDepartamentos.limpaCampos;
begin
  edtID.Clear;
  edtNome.Clear;
  edtLocal.Clear;
end;

procedure TfrmDepartamentos.loadDepartamento;
var
  oDepartamento : TDepartamento;
  oDepartamentoControl : TDepartamentoController;
begin
  oDepartamento         := TDepartamento.Create;
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    oDepartamentoControl.loadDepartamento(oDepartamento, dsDepartamento.DataSet.FieldByName('id_departamento').AsInteger);
    with oDepartamento do
    begin
      edtID.Text    := IntToStr(Id_departamento);
      edtLocal.Text := Local;
      edtNome.Text  := Nm_departamento;
    end;
  finally
    FreeAndNil(oDepartamento);
    FreeAndNil(oDepartamentoControl);
  end;
end;

procedure TfrmDepartamentos.save;
var
  oDepartamentoControl : TDepartamentoController;
begin
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    case FOperation of
      opNew: Insert;
      opEdit: Edit;      
    end;
    oDepartamentoControl.Search(edtPesquisar.Text);
  finally
    FreeAndNil(oDepartamentoControl);
  end;  
end;

procedure TfrmDepartamentos.searchDepartamento;
var
  oDepartamento : TDepartamentoController;
begin
  oDepartamento := TDepartamentoController.Create;
  try
    oDepartamento.Search(edtPesquisar.Text);
  finally
    FreeAndNil(oDepartamento);
  end;
end;

end.
