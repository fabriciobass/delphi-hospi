unit uGer_Empregados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.Mask, Vcl.DBCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  JvExMask, JvToolEdit, JvDBControls, frxClass, frxDBSet;

type
  TOperation  = (opNew, opEdit, opNavigate);

  TfrmEmpregados = class(TForm)
    pgEmpregado: TPageControl;
    tabLista: TTabSheet;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    edtPesquisar: TEdit;
    btnPesquisar: TButton;
    Panel2: TPanel;
    btnEditar: TButton;
    btnExcluir: TButton;
    btnNovo: TButton;
    tabCadastro: TTabSheet;
    Panel3: TPanel;
    btnCancelar: TButton;
    btnGravar: TButton;
    dsEmpregados: TDataSource;
    dsDepartamentos: TDataSource;
    panCadastro: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    txtDepartamento: TLabel;
    edtCodigo: TDBEdit;
    edtCodFuncao: TDBEdit;
    edtNomeEmpregado: TDBEdit;
    Label3: TLabel;
    edtNomeFuncao: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtSalario: TDBEdit;
    EdtComissao: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    dbDepartamento: TDBLookupComboBox;
    qryDepartamento: TFDQuery;
    qryDepartamentoid_departamento: TIntegerField;
    qryDepartamentonm_departamento: TWideStringField;
    qryDepartamentolocal: TWideStringField;
    JvDBDateEdit1: TJvDBDateEdit;
    frxReport: TfrxReport;
    DBEmpregado: TfrxDBDataset;
    btnImprimir: TButton;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnImprimirClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FOperation  : TOperation;
  public
    { Public declarations }
    procedure searchEmpregado;
    procedure Delete;
    procedure save;
    procedure Control(aOperation: TOperation);
  end;

var
  frmEmpregados: TfrmEmpregados;

implementation

{$R *.dfm}

uses uConexao, uEmpregadoController, uEmpregado;

procedure TfrmEmpregados.btnCancelarClick(Sender: TObject);
begin
  searchEmpregado;
  pgEmpregado.ActivePage := tabLista;
  Control(opNavigate);
end;

procedure TfrmEmpregados.btnEditarClick(Sender: TObject);
begin
  FOperation  := opEdit;
  dmConexao.qryEmpregados.Edit;
  pgEmpregado.ActivePage := tabCadastro;
  Control(opEdit);
end;

procedure TfrmEmpregados.btnExcluirClick(Sender: TObject);
begin
  Delete;
end;

procedure TfrmEmpregados.btnGravarClick(Sender: TObject);
begin
  save;
  searchEmpregado;
  Control(opNavigate);
  pgEmpregado.ActivePage := tabLista;
end;

procedure TfrmEmpregados.btnImprimirClick(Sender: TObject);
begin
  if dmConexao.qryEmpregados.RecordCount > 0 then
  begin
    frxReport.Clear;
    frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
      '\relEmpregados.fr3');
    frxReport.ShowReport;
  end else
    raise Exception.Create('N�o existe registos para impress�o!');
end;

procedure TfrmEmpregados.btnNovoClick(Sender: TObject);
begin
  FOperation  := opNew;
  Control(opNew);
  pgEmpregado.ActivePage := tabCadastro;
  if not dmConexao.qryEmpregados.Active then
    dmConexao.qryEmpregados.Active  := True;

  dmConexao.qryEmpregados.Insert;
  edtNomeEmpregado.SetFocus;
end;

procedure TfrmEmpregados.btnPesquisarClick(Sender: TObject);
begin
  searchEmpregado;
end;

procedure TfrmEmpregados.Control(aOperation: TOperation);
begin
  case aOperation of
    opNew, opEdit:
    begin
      panCadastro.Enabled := True;
      btnCancelar.Enabled := True;
      btnGravar.Enabled   := True;
      btnNovo.Enabled     := False;
      btnEditar.Enabled   := False;
      btnExcluir.Enabled  := False;
    end;
    opNavigate:
    begin
      panCadastro.Enabled     := False;
      btnCancelar.Enabled := False;
      btnGravar.Enabled   := False;
      btnNovo.Enabled     := True;
      btnEditar.Enabled   := True;
      btnExcluir.Enabled  := True;
    end;
  end;
end;

procedure TfrmEmpregados.DBGrid1DblClick(Sender: TObject);
begin
  btnEditarClick(Self);
end;

procedure TfrmEmpregados.Delete;
var
  objEmpregadoControl : TEmpregadoController;
  sErro: String;
begin
  objEmpregadoControl  := TEmpregadoController.Create;
  try
    if (dmConexao.qryEmpregados.Active) and (dmConexao.qryEmpregados.RecordCount > 0) then
    begin
      if MessageDlg('Deseja exlcuir o registro?', mtConfirmation, [mbYes, mbNo], 0) = IDYES then
      begin
        if objEmpregadoControl.delete(dmConexao.qryEmpregados.FieldByName('id_empregado').AsInteger, sErro) = False then
          raise Exception.Create(sErro);
        objEmpregadoControl.Search(edtPesquisar.Text);
      end;
    end
    else
      raise Exception.Create('N�o existem registros a serem exclu�dos!');

  finally
    FreeAndNil(objEmpregadoControl);
  end;

end;

procedure TfrmEmpregados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_F2: btnNovoClick(Self);
    VK_F3: btnEditarClick(Self);
    VK_F4: btnExcluirClick(Self);
    VK_F5: btnGravarClick(Self);
    VK_F6: btnCancelarClick(Self);
    VK_ESCAPE: Close;
  end;
end;

procedure TfrmEmpregados.FormKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
end;

procedure TfrmEmpregados.FormShow(Sender: TObject);
begin
  dmConexao.qryDepartamento.Open;
  pgEmpregado.ActivePage := tabLista;
  Control(opNavigate);
end;


procedure TfrmEmpregados.save;
begin
  dmConexao.qryEmpregados.Post;
end;

procedure TfrmEmpregados.searchEmpregado;
var
  objEmpregado : TEmpregadoController;
begin
  objEmpregado := TEmpregadoController.Create;
  try
    objEmpregado.Search(edtPesquisar.Text);
  finally
    FreeAndNil(objEmpregado);
  end;

end;

end.
