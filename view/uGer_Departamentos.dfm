object frmDepartamentos: TfrmDepartamentos
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'GERENCIAMENTO DE DEPARTAMENTO'
  ClientHeight = 571
  ClientWidth = 1018
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgDepartamento: TPageControl
    Left = 0
    Top = 0
    Width = 1018
    Height = 571
    ActivePage = tabLista
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabHeight = 35
    TabOrder = 0
    TabStop = False
    TabWidth = 150
    object tabLista: TTabSheet
      Caption = ' LISTAGEM '
      object DBGrid1: TDBGrid
        Left = 0
        Top = 57
        Width = 1010
        Height = 412
        Align = alClient
        DataSource = dsDepartamento
        DrawingStyle = gdsGradient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'id_departamento'
            Title.Caption = 'C'#243'digo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nm_departamento'
            Title.Caption = 'Departamento'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 500
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'local'
            Title.Caption = 'Local'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 250
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1010
        Height = 57
        Align = alTop
        TabOrder = 0
        object edtPesquisar: TEdit
          Left = 8
          Top = 20
          Width = 449
          Height = 24
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          TextHint = 'Digite para pesquisar'
        end
        object btnPesquisar: TButton
          Left = 463
          Top = 16
          Width = 90
          Height = 35
          Caption = 'Pesquisar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btnPesquisarClick
        end
        object btnImprimir: TButton
          Left = 873
          Top = 12
          Width = 116
          Height = 35
          Caption = 'Imprimir'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btnImprimirClick
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 469
        Width = 1010
        Height = 57
        Align = alBottom
        Color = clSilver
        ParentBackground = False
        TabOrder = 2
        object btnEditar: TButton
          Left = 744
          Top = 16
          Width = 116
          Height = 35
          Caption = '&Editar - F3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btnEditarClick
        end
        object btnExcluir: TButton
          Left = 872
          Top = 16
          Width = 117
          Height = 35
          Caption = 'E&xcluir - F4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btnExcluirClick
        end
        object btnNovo: TButton
          Left = 616
          Top = 16
          Width = 116
          Height = 35
          Caption = '&Novo - F2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = btnNovoClick
        end
      end
    end
    object tabCadastro: TTabSheet
      Caption = ' CADASTRO '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 60
        Top = 25
        Width = 56
        Height = 18
        Caption = 'C'#243'digo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 14
        Top = 55
        Width = 102
        Height = 18
        Caption = 'Departament:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 71
        Top = 85
        Width = 45
        Height = 18
        Caption = 'Local:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtLocal: TEdit
        Left = 122
        Top = 84
        Width = 535
        Height = 24
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object edtNome: TEdit
        Left = 122
        Top = 54
        Width = 535
        Height = 24
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object edtID: TEdit
        Left = 122
        Top = 24
        Width = 141
        Height = 25
        Color = clSilver
        Ctl3D = False
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 0
        Top = 469
        Width = 1010
        Height = 57
        Align = alBottom
        Color = clSilver
        ParentBackground = False
        TabOrder = 3
        object btnCancelar: TButton
          Left = 744
          Top = 16
          Width = 120
          Height = 35
          Caption = '&Cancelar - F6'
          TabOrder = 1
          OnClick = btnCancelarClick
        end
        object btnGravar: TButton
          Left = 880
          Top = 16
          Width = 120
          Height = 35
          Caption = '&Gravar - F5'
          TabOrder = 0
          OnClick = btnGravarClick
        end
      end
    end
  end
  object dsDepartamento: TDataSource
    DataSet = dmConexao.qryDepartamento
    Left = 576
  end
  object frxReport: TfrxReport
    Tag = 1
    Version = '6.9.12'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44424.392304560200000000
    ReportOptions.LastChange = 44424.444667500000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnReportPrint = 'frxReportOnReportPrint'
    Left = 784
    Top = 152
    Datasets = <
      item
        DataSet = DBEmpregado
        DataSetName = 'DBDepartamento'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = [ftBottom]
        Frame.BottomLine.Style = fsDot
        Height = 24.566929130000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 2.834639565000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Relat'#243'rio de Departamentos')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          Formats = <
            item
            end
            item
            end>
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 257.008040000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina: [Page#]')
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 173.858380000000000000
        Width = 718.110700000000000000
        DataSet = DBEmpregado
        DataSetName = 'DBDepartamento'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 3.000000000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBEmpregado."id_empregado"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 3.000000000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBEmpregado."nm_empregado"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 2.779530000000000000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBEmpregado."nm_funcao"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 2.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'data_admissao'
          DataSet = DBEmpregado
          DataSetName = 'DBDepartamento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DBDepartamento."data_admissao"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 2.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'salario'
          DataSet = DBEmpregado
          DataSetName = 'DBDepartamento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.ThousandSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DBDepartamento."salario"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = [ftTop, ftBottom]
        Frame.TopLine.Style = fsDot
        Frame.BottomLine.Style = fsDot
        Height = 45.354330710000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Condition = 'DBDepartamento."id_departamento"'
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Top = 2.834639570000000000
          Width = 555.590910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Departamento: [DBDepartamento."id_departamento"] - [DBDepartamen' +
              'to."nm_departamento"] - [DBDepartamento."local"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Top = 26.456710000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome Empregado')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 26.456710000000000000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fun'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 26.456710000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Admiss'#227'o')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 26.456710000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Sal'#225'rio')
          ParentFont = False
        end
      end
    end
  end
  object DBEmpregado: TfrxDBDataset
    UserName = 'DBDepartamento'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nm_empregado=nm_empregado'
      'id_empregado=id_empregado'
      'id_departamento=id_departamento'
      'nm_departamento=nm_departamento'
      'local=local'
      'salario=salario'
      'data_admissao=data_admissao'
      'comissao=comissao'
      'nm_funcao=nm_funcao')
    DataSet = qryRelDepartamento
    BCDToCurrency = False
    Left = 800
    Top = 208
  end
  object qryRelDepartamento: TFDQuery
    Connection = dmConexao.DMConexao
    SQL.Strings = (
      
        'select e.nm_funcao, e.salario, e.data_admissao, e.comissao, e.nm' +
        '_empregado, e.id_empregado, d.* from departamentos d right join ' +
        'empregados e on d.id_departamento = e.cod_departamento')
    Left = 376
    Top = 192
    object qryRelDepartamentonm_empregado: TWideStringField
      FieldName = 'nm_empregado'
      Origin = 'nm_empregado'
      Size = 100
    end
    object qryRelDepartamentoid_empregado: TIntegerField
      FieldName = 'id_empregado'
      Origin = 'id_empregado'
    end
    object qryRelDepartamentoid_departamento: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_departamento'
      Origin = 'id_departamento'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryRelDepartamentonm_departamento: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'nm_departamento'
      Origin = 'nm_departamento'
      Size = 100
    end
    object qryRelDepartamentolocal: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'local'
      Origin = '"local"'
      Size = 100
    end
    object qryRelDepartamentosalario: TFMTBCDField
      FieldName = 'salario'
      Origin = 'salario'
      Precision = 64
      Size = 5
    end
    object qryRelDepartamentodata_admissao: TDateField
      FieldName = 'data_admissao'
      Origin = 'data_admissao'
    end
    object qryRelDepartamentocomissao: TFMTBCDField
      FieldName = 'comissao'
      Origin = 'comissao'
      Precision = 64
      Size = 5
    end
    object qryRelDepartamentonm_funcao: TWideStringField
      FieldName = 'nm_funcao'
      Origin = 'nm_funcao'
      Size = 100
    end
  end
end
