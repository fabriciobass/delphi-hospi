program ProvaHospidata;

uses
  Vcl.Forms,
  uDepartamento in 'model\uDepartamento.pas',
  uDepartamentoController in 'controller\uDepartamentoController.pas',
  uConexao in 'dao\uConexao.pas' {dmConexao: TDataModule},
  uGer_Departamentos in 'view\uGer_Departamentos.pas' {frmDepartamentos},
  uGer_Empregados in 'view\uGer_Empregados.pas' {frmEmpregados},
  uEmpregado in 'model\uEmpregado.pas',
  uEmpregadoController in 'controller\uEmpregadoController.pas',
  uPrincipal in 'uPrincipal.pas' {frmPrincipal};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmConexao, dmConexao);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
