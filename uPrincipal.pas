unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus;

type
  TfrmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Gerenciamento1: TMenuItem;
    mnEmpregados: TMenuItem;
    mnDepartamentos: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure mnEmpregadosClick(Sender: TObject);
    procedure mnDepartamentosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses uGer_Empregados, uGer_Departamentos, uConexao;

procedure TfrmPrincipal.Button1Click(Sender: TObject);
var
  frmGer_Empregado : TfrmEmpregados;
begin
  frmGer_Empregado := TfrmEmpregados.Create(nil);
  try
    frmGer_Empregado.ShowModal;
  finally
    FreeAndNil(frmGer_Empregado);
  end;
end;

procedure TfrmPrincipal.mnDepartamentosClick(Sender: TObject);
var
  frmGer_Departamento : TfrmDepartamentos;
begin
  frmGer_Departamento := TfrmDepartamentos.Create(nil);
  try
    frmGer_Departamento.ShowModal;
  finally
    FreeAndNil(frmGer_Departamento);
  end;

end;

procedure TfrmPrincipal.mnEmpregadosClick(Sender: TObject);
var
  frmGer_Empregado : TfrmEmpregados;
begin
  frmGer_Empregado := TfrmEmpregados.Create(nil);
  try
    frmGer_Empregado.ShowModal;
  finally
    FreeAndNil(frmGer_Empregado);
  end;

end;

end.
