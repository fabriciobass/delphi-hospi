object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Sistema Teste HospiData - 2021'
  ClientHeight = 378
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 304
    Top = 96
    object Gerenciamento1: TMenuItem
      Caption = 'Gerenciamento'
      object mnEmpregados: TMenuItem
        Caption = 'Empregados'
        OnClick = mnEmpregadosClick
      end
      object mnDepartamentos: TMenuItem
        Caption = 'Departamentos'
        OnClick = mnDepartamentosClick
      end
    end
  end
end
