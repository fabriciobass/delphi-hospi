unit uEmpregado;

interface

uses
  System.SysUtils;

type
  TEmpregado = class
  private
    FId_empregado: integer;
    FCod_departamento: Integer;
    FSalario: Double;
    FData_admissao: TDateTime;
    FCod_emp_funcao: Integer;
    FComissao: Double;
    FNm_empregado: string;
    FNm_funcao: string;
    { private declarations }
  public
    { public declarations }
    property Id_empregado: integer read FId_empregado write FId_empregado;
    property Cod_departamento: Integer read FCod_departamento write FCod_departamento;
    property Cod_emp_funcao: Integer read FCod_emp_funcao write FCod_emp_funcao;
    property Nm_empregado: string read FNm_empregado write FNm_empregado;
    property Nm_funcao: string read FNm_funcao write FNm_funcao;
    property Data_admissao: TDateTime read FData_admissao write FData_admissao;
    property Salario: Double read FSalario write FSalario;
    property Comissao: Double read FComissao write FComissao;
  end;

implementation

{ TDepartamento }



end.
