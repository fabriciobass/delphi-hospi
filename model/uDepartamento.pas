unit uDepartamento;

interface

uses
  System.SysUtils;

type
  TDepartamento = class
  private
    FLocal: string;
    FNm_departamento: string;
    FId_departamento: integer;
    procedure SetNm_departamento(const Value: string);
    { private declarations }
  public
    { public declarations }
    property Id_departamento: integer read FId_departamento write FId_departamento;
    property Nm_departamento: string read FNm_departamento write SetNm_departamento;
    property Local: string read FLocal write FLocal;
  end;

implementation

{ TDepartamento }

procedure TDepartamento.SetNm_departamento(const Value: string);
begin
  if Value = EmptyStr then
    raise Exception.Create('''Nome Departamento precisa ser preenchido"');

  FNm_departamento := Value;
end;

end.
