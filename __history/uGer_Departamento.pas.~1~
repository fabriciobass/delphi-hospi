unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TOperation  = (opNew, opEdit, opNavigate);
  
  TfrmDepartamento = class(TForm)
    dsDepartamento: TDataSource;
    pgDepartamento: TPageControl;
    tabLista: TTabSheet;
    tabCadastro: TTabSheet;
    DBGrid1: TDBGrid;
    edtLocal: TEdit;
    edtNome: TEdit;
    edtID: TEdit;
    Panel1: TPanel;
    edtPesquisar: TEdit;
    btnPesquisar: TButton;
    Panel2: TPanel;
    btnEditar: TButton;
    btnExcluir: TButton;
    Panel3: TPanel;
    btnCancelar: TButton;
    btnGravar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnNovo: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FOperation  : TOperation;
  public
    { Public declarations }
    procedure searchDepartamento;
    procedure loadDepartamento;
    procedure Edit;
    procedure Delete;
    procedure Insert;
    procedure save;
    procedure Control(aOperation: TOperation);
    procedure limpaCampos;
  end;

var
  frmDepartamento: TfrmDepartamento;

implementation

{$R *.dfm}

uses uConexao, uDepartamentoController, uDepartamento;

procedure TfrmDepartamento.btnGravarClick(Sender: TObject);
begin
  if edtNome.Text = '' then
  begin
    ShowMessage('Por favor, digite um Nome');
    Exit;
    edtNome.SetFocus;
  end;
  if edtLocal.Text = '' then
  begin
    ShowMessage('Por favor, digite um Local');
    Exit;
    edtLocal.SetFocus;
  end;
  save;
  Control(opNavigate);
  pgDepartamento.ActivePage := tabLista;
end;

procedure TfrmDepartamento.Button2Click(Sender: TObject);
begin
  Edit;
end;

procedure TfrmDepartamento.btnEditarClick(Sender: TObject);
begin
  FOperation  := opEdit;
  loadDepartamento;
  pgDepartamento.ActivePage := tabCadastro;
  Control(opEdit);  
end;

procedure TfrmDepartamento.btnExcluirClick(Sender: TObject);
begin
  Delete;
end;

procedure TfrmDepartamento.btnPesquisarClick(Sender: TObject);
begin
  searchDepartamento;
end;

procedure TfrmDepartamento.btnCancelarClick(Sender: TObject);
begin
  searchDepartamento;
  pgDepartamento.ActivePage := tabLista;
  Control(opNavigate);
end;

procedure TfrmDepartamento.btnNovoClick(Sender: TObject);
begin
  
  FOperation  := opNew;
  Control(opNew);
  limpaCampos;  
  pgDepartamento.ActivePage := tabCadastro;
  edtID.Text  := IntToStr(dmConexao.getNextID); 
  edtNome.SetFocus;
  
end;

procedure TfrmDepartamento.Control(aOperation: TOperation);
begin
  case aOperation of
    opNew, opEdit:
    begin
      edtNome.Enabled     := True;
      edtLocal.Enabled    := True;
      btnCancelar.Enabled := True;
      btnGravar.Enabled   := True;
      btnNovo.Enabled     := False;
      btnEditar.Enabled   := False;
      btnExcluir.Enabled  := False;
    end;
    opNavigate:
    begin
      edtNome.Enabled     := False;
      edtLocal.Enabled    := False;
      btnCancelar.Enabled := False;
      btnGravar.Enabled   := False;
      btnNovo.Enabled     := True;
      btnEditar.Enabled   := True;
      btnExcluir.Enabled  := True;
    end;
  end;  
end;

procedure TfrmDepartamento.DBGrid1DblClick(Sender: TObject);
begin
  btnEditarClick(Self);
end;

procedure TfrmDepartamento.Delete;
var
  oDepartamentoControl : TDepartamentoController;
  sErro: String;
begin
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    if (dmConexao.qryDepartamento.Active) and (dmConexao.qryDepartamento.RecordCount > 0) then
    begin
      if MessageDlg('Deseja exlcuir o registro?', mtConfirmation, [mbYes, mbNo], 0) = IDYES then
      begin
        if oDepartamentoControl.delete(dmConexao.qryDepartamento.FieldByName('id_departamento').AsInteger, sErro) = False then
          raise Exception.Create(sErro);
        oDepartamentoControl.Search(edtPesquisar.Text);
      end;
    end
    else
      raise Exception.Create('N�o existem registros a serem exclu�dos!');
        
  finally
    FreeAndNil(oDepartamentoControl);
  end;
end;

procedure TfrmDepartamento.Edit;
var
  ODepartamento : TDepartamento;
  ODepartamentoControl : TDepartamentoController;
  sErro: string;
begin
  ODepartamento := TDepartamento.Create;
  ODepartamentoControl  := TDepartamentoController.Create;
  try
    with ODepartamento do
    begin
      Id_departamento := dmConexao.qryDepartamento.FieldByName('id_departamento').AsInteger;
      Nm_departamento := edtNome.Text;
      Local := edtLocal.Text;
    end;
    if ODepartamentoControl.Edit(ODepartamento, sErro) = False then
      raise Exception.Create(sErro);

  finally
    FreeAndNil(ODepartamento);
    FreeAndNil(ODepartamentoControl);
  end;
end;

procedure TfrmDepartamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;
  
end;

procedure TfrmDepartamento.FormShow(Sender: TObject);
begin
  pgDepartamento.ActivePage := tabLista;
end;

procedure TfrmDepartamento.Insert;
var
  oDepartamento : TDepartamento;
  oDepartamentoControl : TDepartamentoController;
  sErro: string;
begin
  oDepartamento := TDepartamento.Create;
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    with oDepartamento do
    begin  
      Id_departamento := 0;
      Nm_departamento := edtNome.Text;
      Local := edtLocal.Text;
    end;
    if oDepartamentoControl.Insert(oDepartamento, sErro) = False then
      raise Exception.Create(sErro);
    
  finally
    FreeAndNil(oDepartamento);
    FreeAndNil(oDepartamentoControl);    
  end;
end;

procedure TfrmDepartamento.limpaCampos;
begin
  edtID.Clear;
  edtNome.Clear;
  edtLocal.Clear;
end;

procedure TfrmDepartamento.loadDepartamento;
var
  oDepartamento : TDepartamento;
  oDepartamentoControl : TDepartamentoController;
begin
  oDepartamento         := TDepartamento.Create;
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    oDepartamentoControl.loadDepartamento(oDepartamento, dsDepartamento.DataSet.FieldByName('id_departamento').AsInteger);
    with oDepartamento do
    begin
      edtID.Text    := IntToStr(Id_departamento);
      edtLocal.Text := Local;
      edtNome.Text  := Nm_departamento;
    end;
  finally
    FreeAndNil(oDepartamento);
    FreeAndNil(oDepartamentoControl);
  end;
end;

procedure TfrmDepartamento.save;
var
  oDepartamentoControl : TDepartamentoController;
begin
  oDepartamentoControl  := TDepartamentoController.Create;
  try
    case FOperation of
      opNew: Insert;
      opEdit: Edit;      
    end;
    oDepartamentoControl.Search(edtPesquisar.Text);
  finally
    FreeAndNil(oDepartamentoControl);
  end;  
end;

procedure TfrmDepartamento.searchDepartamento;
var
  oDepartamento : TDepartamentoController;
begin
  oDepartamento := TDepartamentoController.Create;
  try
    oDepartamento.Search(edtPesquisar.Text);
  finally
    FreeAndNil(oDepartamento);
  end;
end;

end.
