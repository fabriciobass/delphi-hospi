unit uEmpregadoController;

interface

uses uEmpregado, System.SysUtils;

type
  TEmpregadoController = class
  private
    { private declarations }
  public
    { public declarations }
    function Insert(objEmpregado: TEmpregado; out sErro: string): Boolean;
    function Edit(objEmpregado: TEmpregado; out sErro: string): Boolean;
    function delete(iCodigo: Integer; out sErro: string): Boolean;
    procedure Search(nmEmpregado: string);

  end;

implementation



{ TDepartamentoController }

uses uConexao;

function TEmpregadoController.delete(iCodigo: Integer;
  out sErro: string): Boolean;
begin
  with dmConexao.qryEmpregados do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('delete from empregados where id_empregado = :id_empregado ' );
    Params[0].AsInteger   := iCodigo;
    try
      ExecSQL;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Alterar o Empregado: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

function TEmpregadoController.Edit(objEmpregado: TEmpregado;
  out sErro: string): Boolean;
begin
  with dmConexao.qryEmpregados do
  begin
  if Active then
    Close;
    try
      Post;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Alterar o Empregado: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

function TEmpregadoController.Insert(objEmpregado: TEmpregado;
  out sErro: string): Boolean;
begin
  with dmConexao.qryEmpregados do
  begin
  if Active then
    Close;

    try
      Post;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Inserir o Empregado: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

procedure TEmpregadoController.Search(nmEmpregado: string);
begin
  with dmConexao.qryEmpregados do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('select e.*, d.nm_departamento from empregados e ');
    SQL.Add('Left join departamentos d on d.id_departamento = e.cod_departamento ' );
    SQL.Add('where e.nm_empregado like :nm_empregado' );
    Params[0].AsString  := '%' + nmEmpregado + '%';
    Open;
    First;
  end;


end;

end.
