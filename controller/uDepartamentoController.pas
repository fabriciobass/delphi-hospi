unit uDepartamentoController;

interface

uses uDepartamento, System.SysUtils;

type
  TDepartamentoController = class
  private
    { private declarations }
  public
    { public declarations }
    function Insert(oDepartamento: TDepartamento; out sErro: string): Boolean;
    function Edit(oDepartamento: TDepartamento; out sErro: string): Boolean;
    function delete(iCodigo: Integer; out sErro: string): Boolean;
    procedure Search(nmDepart: string);
    procedure loadDepartamento(oDepartamento: TDepartamento; iID: Integer);

  end;

implementation



{ TDepartamentoController }

uses uConexao;

function TDepartamentoController.delete(iCodigo: Integer;
  out sErro: string): Boolean;
begin
  with dmConexao.qryDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('delete from departamentos where id_departamento = :id_departamento ' );
    Params[0].AsInteger   := iCodigo;
    try
      ExecSQL;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Alterar o Departamento: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

function TDepartamentoController.Edit(oDepartamento: TDepartamento;
  out sErro: string): Boolean;
begin
  with dmConexao.qryDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('update departamentos set nm_departamento = :nm_departamento, local = :local ' );
    SQL.Add('where id_departamento = :id_departamento');
    Params[0].AsString    := oDepartamento.Nm_departamento;
    Params[1].AsString    := oDepartamento.Local;
    Params[2].AsInteger   := oDepartamento.Id_departamento;
    try
      ExecSQL;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Alterar o Departamento: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

function TDepartamentoController.Insert(oDepartamento: TDepartamento;
  out sErro: string): Boolean;
begin
  with dmConexao.qryDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('insert into departamentos (id_departamento, nm_departamento, local) VALUES ');
    sql.Add('(:id_departamento, :nm_departamento, :local)');
    Params[0].AsInteger   := dmConexao.getNextID('departamentos');
    Params[1].AsString    := oDepartamento.Nm_departamento;
    Params[2].AsString    := oDepartamento.Local;
    try
      ExecSQL;
      Result  := True;
    except on E: Exception do
    begin
      sErro   := 'Erro ao Inserir o Departamento: ' + sLineBreak + E.Message;
      Result  := False;
    end;
    end;
  end;
end;

procedure TDepartamentoController.loadDepartamento(oDepartamento: TDepartamento;
  iID: Integer);
begin
  with dmConexao.qryDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('select * from departamentos where id_departamento = :id_departamento');
    Params[0].AsInteger  := iID;
    Open;
    with oDepartamento do
    begin
      Id_departamento := FieldByName('id_departamento').AsInteger;
      nm_departamento := FieldByName('nm_departamento').AsString;
      local           := FieldByName('local').AsString;
    end;
  end;
end;

procedure TDepartamentoController.Search(nmDepart: string);
begin
  with dmConexao.qryDepartamento do
  begin
  if Active then
    Close;

    SQL.Clear;
    SQL.Add('select * from departamentos where nm_departamento like :nm_departamento' );
    Params[0].AsString  := '%' + nmDepart + '%';
    Open;
    First;
  end;


end;

end.
