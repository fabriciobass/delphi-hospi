object dmConexao: TdmConexao
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 534
  Width = 726
  object DMConexao: TFDConnection
    Params.Strings = (
      'Database=hospidata'
      'User_Name=postgres'
      'Password=admin'
      'Server=localhost'
      'DriverID=PG')
    LoginPrompt = False
    Left = 56
    Top = 48
  end
  object PGDriver: TFDPhysPgDriverLink
    VendorLib = 'C:\Program Files (x86)\PostgreSQL\10\bin\libpq.dll'
    Left = 56
    Top = 112
  end
  object qryDepartamento: TFDQuery
    Connection = DMConexao
    SQL.Strings = (
      'select * from departamentos d')
    Left = 128
    Top = 48
    object qryDepartamentoid_departamento: TIntegerField
      FieldName = 'id_departamento'
      Origin = 'id_departamento'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryDepartamentonm_departamento: TWideStringField
      FieldName = 'nm_departamento'
      Origin = 'nm_departamento'
      Size = 100
    end
    object qryDepartamentolocal: TWideStringField
      FieldName = 'local'
      Origin = '"local"'
      Size = 100
    end
  end
  object qryEmpregados: TFDQuery
    Connection = DMConexao
    SQL.Strings = (
      
        'select e.*, d.nm_departamento from empregados e left join depart' +
        'amentos d on d.id_departamento = e.cod_departamento')
    Left = 128
    Top = 96
    object qryEmpregadosid_empregado: TIntegerField
      FieldName = 'id_empregado'
      Origin = 'id_empregado'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object qryEmpregadoscod_departamento: TIntegerField
      FieldName = 'cod_departamento'
      Origin = 'cod_departamento'
    end
    object qryEmpregadoscod_emp_funcao: TIntegerField
      FieldName = 'cod_emp_funcao'
      Origin = 'cod_emp_funcao'
    end
    object qryEmpregadosnm_empregado: TWideStringField
      FieldName = 'nm_empregado'
      Origin = 'nm_empregado'
      Size = 100
    end
    object qryEmpregadosnm_funcao: TWideStringField
      FieldName = 'nm_funcao'
      Origin = 'nm_funcao'
      Size = 100
    end
    object qryEmpregadosdata_admissao: TDateField
      FieldName = 'data_admissao'
      Origin = 'data_admissao'
    end
    object qryEmpregadossalario: TFMTBCDField
      FieldName = 'salario'
      Origin = 'salario'
      DisplayFormat = ',0.00'
      Precision = 64
      Size = 5
    end
    object qryEmpregadoscomissao: TFMTBCDField
      FieldName = 'comissao'
      Origin = 'comissao'
      DisplayFormat = ',0.00'
      Precision = 64
      Size = 5
    end
    object qryEmpregadosnm_departamento: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'nm_departamento'
      Origin = 'nm_departamento'
      Size = 100
    end
  end
  object Transacao: TFDTransaction
    Connection = DMConexao
    Left = 56
    Top = 168
  end
end
