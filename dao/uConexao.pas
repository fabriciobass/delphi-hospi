unit uConexao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.PG, FireDAC.Phys.PGDef,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, IniFiles, Vcl.Forms, Dialogs;

type
  TdmConexao = class(TDataModule)
    DMConexao: TFDConnection;
    PGDriver: TFDPhysPgDriverLink;
    qryDepartamento: TFDQuery;
    qryDepartamentoid_departamento: TIntegerField;
    qryDepartamentonm_departamento: TWideStringField;
    qryDepartamentolocal: TWideStringField;
    qryEmpregados: TFDQuery;
    qryEmpregadosid_empregado: TIntegerField;
    qryEmpregadoscod_departamento: TIntegerField;
    qryEmpregadoscod_emp_funcao: TIntegerField;
    qryEmpregadosnm_empregado: TWideStringField;
    qryEmpregadosnm_funcao: TWideStringField;
    qryEmpregadosdata_admissao: TDateField;
    qryEmpregadossalario: TFMTBCDField;
    qryEmpregadoscomissao: TFMTBCDField;
    Transacao: TFDTransaction;
    qryEmpregadosnm_departamento: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function getNextID(tabela: string): Integer;
  end;

var
  dmConexao: TdmConexao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDataModule1 }

procedure TdmConexao.DataModuleCreate(Sender: TObject);
var
  iArq: TIniFile;
begin
  try
    iArq := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Banco.ini');

    DMConexao.Params.Values['DriverID'] := 'PG';
    DMConexao.Params.Values['Server'] := iArq.ReadString('BD', 'IP', '');
    DMConexao.Params.Values['Database'] := iArq.ReadString('BD', 'DATA', '');
    DMConexao.Params.Values['Password'] := iArq.ReadString('BD', 'PASS', '');
    DMConexao.Params.Values['User_name'] := iArq.ReadString('BD', 'USER', '');

    try
      DMConexao.Connected := true;
    Except
      ShowMessage('N�o foi possivel conectar na base de dados!');
      Application.Terminate;
    end;

  Finally
    iArq.Free;
  end;

end;

function TdmConexao.getNextID(tabela: string): Integer;
var
  sqlNextID: TFDQuery;
begin
  sqlNextID := TFDQuery.Create(nil);
  try
    with sqlNextID do
    begin
      Connection  := dMConexao;
      SQL.Clear;
      SQL.Add('select coalesce(max(id_departamento),0) + 1 as id from ' + tabela);
      Open;
      Result  := FieldByName('id').AsInteger;
    end;
  finally
    FreeAndNil(sqlNextID);
  end;
end;

end.
